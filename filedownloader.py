#Remote file downloader
#Downloads sequential files into a new directory based on the filename.

from os import path
from os import makedirs
from urllib import urlretrieve
from urlparse import urlparse

URLPrefix = raw_input("URL prefix?") #The URL of the file before the digits.
prefixDigits = int(raw_input("Number of digits?")) #Number of digits used in the filename.
startNum = int(raw_input("Start number?")) #Ask which number to start on.
maxNum = int(raw_input("Count until?")) #Asks which number to count to.
fileFormat = raw_input("File format?") #Asks the file format. Without period.

zeros = "0000000" #Padding. Needs to be done in an automatic way.

for currentNum in range(startNum,maxNum + 1): #Sets for to go through all files by increasing number.
    currentFile = (zeros + str(currentNum))[-prefixDigits:] #Name of current file. Adds extra zeros if needed.
    
    url = URLPrefix + currentFile + '.' + fileFormat #Sets full URL to download
    targetFile = path.split(urlparse(url).path)[-1] #Gets the filename of the current download
    print "Downloading... " + targetFile #Announces each download as it happens
    if not path.exists(targetFile[:-(prefixDigits + len(fileFormat) + 1)]): #Checks if directory exists for download. If not, create it. Based on filename prefix.
        makedirs(targetFile[:-(prefixDigits + len(fileFormat) + 1)])
    urlretrieve(url, targetFile[:-(prefixDigits + len(fileFormat) + 1)] + '/' + targetFile) #Downloads file into new directory

print "Download finished! :D "
